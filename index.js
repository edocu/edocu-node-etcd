var Etcd = require('node-etcd'),
    etcdUrl = process.env.ETCD_URL,
    etcd = new Etcd(etcdUrl);

module.exports = {
    baseUrl: getKey('/edocu/config/base-url') || 'https://edocu.eu',
    landingPageUrl: getKey('/edocu/config/landing-page-url') || 'http://www.edocu.eu',
    filesStorageUrl: getKey('/edocu/config/files-storage-url') || 'https://api.edocu.eu',
    email: {
        address: {
            contact: getKey('/edocu/config/email/address/contact') || 'info@edocu.eu',
            notification: getKey('/edocu/config/email/address/notification') || 'notifications@edocu.eu',
        },
        smtp: getKey('/edocu/config/email/address/notification'),
    },
    image: {
        headerLogo: getKey('/edocu/config/image/header-logo'),
        emailLogo: getKey('/edocu/config/image/email-logo'),
    },
    css: {
        mainCssPath: getKey('/edocu/config/css/main')
    },
    integration: {
        env: getKey('/edocu/config/integration/env')
    }
}

function getKey(key) {
    var result = etcd.getSync(key);

    result = (result.body && result.body.node && result.body.node.value) ? result.body.node.value : undefined;

    return result;
}
